
function getCurrentDate() {
    const currentDate = new Date();
    const months = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'
    ];
    const month = months[currentDate.getMonth()];
    const day = currentDate.getDate();
    const year = currentDate.getFullYear();
    return `${month} ${day}, ${year}`;
}

function updateDate() {
    const currentDateDiv = document.getElementById('currentDate');
    if (currentDateDiv) {
        currentDateDiv.textContent = getCurrentDate();
    }
}
updateDate();
setInterval(updateDate, 1000);

function getCurrentTime() {
    const currentDate = new Date();
    const hours = currentDate.getHours().toString().padStart(2, '0');
    const minutes = currentDate.getMinutes().toString().padStart(2, '0');

    return `${hours}:${minutes}`;
}

function updateTime() {
    const currentTimeDiv = document.getElementById('currentTime');
    if (currentTimeDiv) {
        currentTimeDiv.textContent = getCurrentTime();
    }
}
updateTime();
setInterval(updateTime, 1000);

function confirmDelete() {
    const sureRemoveDiv = document.createElement('div');
    sureRemoveDiv.classList.add('sure-remove');
    sureRemoveDiv.innerHTML = `
         <div class="overlap-group">
           <div class="rectangle"></div>
           <div class="text-wrapper">NO</div>
         </div>
         <div class="overlap"><div class="div">CANCEL</div></div>
         <div class="overlap-2">
           <div class="rectangle-2"></div>
           <div class="text-wrapper-2">YES</div>
         </div>
         <p class="p">Are you sure you want to remove the computer?</p>
       `;
    sureRemoveDiv.querySelector('.text-wrapper').addEventListener('click', () => sureRemoveDiv.remove());
    sureRemoveDiv.querySelector('.overlap .div').addEventListener('click', () => sureRemoveDiv.remove());
    sureRemoveDiv.querySelector('.overlap-2 .text-wrapper-2').addEventListener('click', () => {
        sureRemoveDiv.remove();
    });
    document.body.appendChild(sureRemoveDiv);
}

document.addEventListener("DOMContentLoaded", function () {
    const deleteClick = document.querySelectorAll('.delete-click');
    deleteClick.forEach(deleteClick => {
        deleteClick.addEventListener('click', confirmDelete);
    });
});
function openWriteMessage() {
    const writeMessageDiv = document.createElement('div');
    writeMessageDiv.classList.add('write-message');
    writeMessageDiv.innerHTML = `
    <div class="text-wrapper">Write message:</div>
    <input type="text" class="message-input" placeholder="I" value="">
    <div class="rectangle-below-text"></div>
    <div class="overlap-group">
          <div class="div"></div>
          <div class="text-wrapper-2">Send</div>
        </div>
        <div class="overlap-2">
          <div class="rectangle-2"></div>
          <div class="text-wrapper-3">Discard</div>
        </div>
      `;
    writeMessageDiv.querySelector('.overlap-group .text-wrapper-2').addEventListener('click', () => writeMessageDiv.remove());
    writeMessageDiv.querySelector('.overlap-2 .text-wrapper-3').addEventListener('click', () => writeMessageDiv.remove());
    document.body.appendChild(writeMessageDiv);
}
document.addEventListener("DOMContentLoaded", function () {
    const sendMessageDiv = document.querySelectorAll('.send-message');
    sendMessageDiv.forEach(sendMessageDiv => {
        sendMessageDiv.addEventListener('click', openWriteMessage);
    });
});
function openSendFile() {
    const sendFile = document.createElement('div');
    sendFile.classList.add('send-file');
    sendFile.innerHTML = `
        <div class="upload-files">
            <div class="overlap">
                <div class="text-wrapper">Send</div>
            </div>
            <div class="overlap-group">
                <div class="div"></div>
                <div class="text-wrapper-2">Discrad</div>
            </div>
            <div class="overlap-2">
                <div class="rectangle-2"></div>
                <img class="box-arrow-in-down" src="img/box-arrow-in-down-1.svg" />
            </div>
            <p class="p">To upload files from your computer, drag them here</p>
        </div>
      `;
    sendFile.querySelector('.overlap .text-wrapper').addEventListener('click', () => sendFile.remove());
    sendFile.querySelector('.overlap-group .text-wrapper-2').addEventListener('click', () => sendFile.remove());
    document.body.appendChild(sendFile);
}
document.addEventListener("DOMContentLoaded", function () {
    const sendFileDivs = document.querySelectorAll('.send-file');
    sendFileDivs.forEach(sendFileDiv => {
        sendFileDiv.addEventListener('click', openSendFile);
    });
});

function openInstallFile() {
    const installFile = document.createElement('div');
    installFile.classList.add('send-file');
    installFile.innerHTML = `
        <div class="upload-files">
            <div class="overlap">
                <div class="text-wrapper">Send</div>
            </div>
            <div class="overlap-group">
                <div class="div"></div>
                <div class="text-wrapper-2">Discrad</div>
            </div>
            <div class="overlap-2">
                <div class="rectangle-2"></div>
                <img class="box-arrow-in-down" src="img/box-arrow-in-down-1.svg" />
            </div>
            <p class="p">Drag the file you want to install here</p>
        </div>
      `;
    installFile.querySelector('.overlap .text-wrapper').addEventListener('click', () => installFile.remove());
    installFile.querySelector('.overlap-group .text-wrapper-2').addEventListener('click', () => installFile.remove());
    document.body.appendChild(installFile);
}
document.addEventListener("DOMContentLoaded", function () {
    const installFileDivs = document.querySelectorAll('.computer-install');
    installFileDivs.forEach(installFileDivs => {
        installFileDivs.addEventListener('click', openInstallFile);
    });
});
